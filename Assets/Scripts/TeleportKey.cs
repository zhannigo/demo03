using UnityEngine;

public class TeleportKey : MonoBehaviour
{
  private void OnTriggerEnter(Collider other)
  {
    if (other.TryGetComponent(out Player player))
    {
      player.PickUpTeleportKey();
      Destroy(gameObject);
    }
  }
}
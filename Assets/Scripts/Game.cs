﻿using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class Game : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _timerValue;
    [SerializeField] private Text _timerView;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exitFromLevel;
    [SerializeField] private Teleport _teleport;
    [SerializeField] private Transform _teleportPoint;
    
    private float _timer = 0;
    private bool _gameIsEnded = false;

    private void Awake()
    {
        _timer = _timerValue;
    }

    private void Start()
    {
        _exitFromLevel.Close();
        if(_teleport!=null)
            _teleport.Close();
    }

    private void Update()
    {
        if(_gameIsEnded)
            return;
        
        TimerTick();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
        TryCompleteLevel();
        if(_teleport!=null)
            TryTeleporting();
    }

    private void TimerTick()
    {
        if(_timerIsOn == false)
            return;
        
        _timer -= Time.deltaTime;
        _timerView.text = $"{_timer:F1}";
        
        if(_timer <= 0)
            Lose();
    }

    private void TryTeleporting()
    {
       if(_teleport.IsOpen == false)
           return;
       var flatTeleportPosition = new Vector2(_teleport.transform.position.x, _teleport.transform.position.z);
       var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);

       if (flatTeleportPosition == flatPlayerPosition)
           Teleporting();
    }

    private void TryCompleteLevel()
    {
        if(_exitFromLevel.IsOpen == false)
            return;

        var flatExitPosition = new Vector2(_exitFromLevel.transform.position.x, _exitFromLevel.transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);
        
        if(flatExitPosition == flatPlayerPosition)
            Victory();
    }

    private void LookAtPlayerHealth()
    {
        if(_player.IsAlive)
            return;

        Lose();
        Destroy(_player.gameObject);
    }

    private void LookAtPlayerInventory()
    {
        if (_player.HasTeleportKey)
            _teleport.Open();
        if(_player.HasKey)
            _exitFromLevel.Open();
    }

    private void Teleporting()
    {
        var flatExitPosition = new Vector3(_teleportPoint.transform.position.x, 0.5f, _teleportPoint.transform.position.z);
        _player.transform.position = flatExitPosition;
    }

    public void Victory()
    {
        _gameIsEnded = true;
        _player.Disable();
        Debug.LogError("Victory");
    }

    public void Lose()
    {
        _gameIsEnded = true;
        _player.Disable();
        Debug.LogError("Lose");
    }
}
